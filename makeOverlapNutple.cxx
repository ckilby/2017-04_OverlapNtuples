// To use this script:
//   root
//   .L makeOverlapNtuple.cxx++
//   makeAllNtuples()

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"

#include <iostream>

struct info {
  info (std::string inputPathIn, std::string outputPathIn) : inputPath(inputPathIn), outputPath(outputPathIn) {
    file    = TFile::Open( inputPathIn.c_str() );
    nomTree = (TTree*) file->Get("nominal_Loose");
  }

  std::string inputPath;
  std::string outputPath;
  TFile* file;
  TTree* nomTree;
  double sumWeightEvents;
};



void makeOverlapNtuple(info& infoIn) {
  std::cout << "\tOutput = " << infoIn.outputPath << std::endl;
  
  ULong64_t  evNum     = 0;
  UInt_t     mcChanNum = 0;
  Char_t     higgsDec  = 0;
  Int_t      regNum    = 0;

  Int_t nJets = 0;
  Int_t nTaus = 0;

  Float_t weight_normalise = 0;
  Float_t weight_mc        = 0;
  Float_t weight_pileup    = 0;
  Float_t weight_lepton    = 0;
  Float_t weight_bTag      = 0;
  Float_t weight_jvt       = 0;

  // Additional ttH(bb) specific weights, not included in evtWeight
  Float_t weight_ttbar_FracRw   = 0;
  Float_t weight_ttbb_Norm      = 0;
  Float_t weight_ttbb_Shape     = 0;
  Float_t weight_dilepFakesNorm = 0;

  Float_t evtWeight = 0;

  Int_t category = 0;

  TFile* outFile = new TFile(infoIn.outputPath.c_str(), "RECREATE");
  TTree* outTree = new TTree("physics", "physics");

  std::cout << "\tSetting branch address for input tree" << std::endl;
  infoIn.nomTree->SetBranchAddress("eventNumber",         &evNum);
  infoIn.nomTree->SetBranchAddress("mcChannelNumber",     &mcChanNum);
  infoIn.nomTree->SetBranchAddress("truth_HDecay",        &higgsDec);
  infoIn.nomTree->SetBranchAddress("DilepRegion_A3",      &regNum);
  infoIn.nomTree->SetBranchAddress("nJets",               &nJets);
  infoIn.nomTree->SetBranchAddress("nTaus",               &nTaus);
  infoIn.nomTree->SetBranchAddress("weight_normalise",         &weight_normalise);
  infoIn.nomTree->SetBranchAddress("weight_mc",                &weight_mc);
  infoIn.nomTree->SetBranchAddress("weight_pileup",            &weight_pileup);
  infoIn.nomTree->SetBranchAddress("weight_leptonSF",          &weight_lepton);
  infoIn.nomTree->SetBranchAddress("weight_bTagSF_Continuous", &weight_bTag);
  infoIn.nomTree->SetBranchAddress("weight_jvt",               &weight_jvt);
  infoIn.nomTree->SetBranchAddress("weight_ttbar_FracRw",      &weight_ttbar_FracRw);
  infoIn.nomTree->SetBranchAddress("weight_ttbb_Norm",         &weight_ttbb_Norm);
  infoIn.nomTree->SetBranchAddress("weight_ttbb_Shape_SherpaNominal", &weight_ttbb_Shape);
  infoIn.nomTree->SetBranchAddress("weight_dilepFakesNorm",    &weight_dilepFakesNorm);

  std::cout << "\tCreating branches for output tree" << std::endl;
  outTree->Branch("event",          &evNum);
  outTree->Branch("channelNum",     &mcChanNum);
  outTree->Branch("category",       &category);
  outTree->Branch("higgsDecayMode", &higgsDec);
  outTree->Branch("weight_normalise",         &weight_normalise);
  outTree->Branch("weight_mc",                &weight_mc);
  outTree->Branch("weight_pileup",            &weight_pileup);
  outTree->Branch("weight_leptonSF",          &weight_lepton);
  outTree->Branch("weight_bTagSF_Continuous", &weight_bTag);
  outTree->Branch("weight_jvt",               &weight_jvt);
  outTree->Branch("evtWeight",                &evtWeight);
  outTree->Branch("weight_ttbar_FracRw",      &weight_ttbar_FracRw);
  outTree->Branch("weight_ttbb_Norm",         &weight_ttbb_Norm);
  outTree->Branch("weight_ttbb_Shape_SherpaNominal", &weight_ttbb_Shape);
  outTree->Branch("weight_dilepFakesNorm",    &weight_dilepFakesNorm);

  std::cout << "\tIterating over input tree" << std::endl;
  std::cout << "\t\t0%" << std::endl;
  int nEntries = infoIn.nomTree->GetEntries();
  int percentDoneCheck = 1; // Variable to test against whether fraction of events run has passed a desired threshold
                            // Currently in tens i.e. 1 == 10%
  for (int iEntry = 0; iEntry < nEntries; ++iEntry) {
    infoIn.nomTree->GetEntry(iEntry);
    
    if (nJets >= 4 && (regNum == 1 || regNum == 3 || regNum == 2) && nTaus == 0 ) { // Only look at signal regions, with tau veto applied
      category = 80000 + 0000 + 100 + 40 + regNum; // 8 + 0=bb + 1=dil + 4=4jet region + reg label

      evtWeight = weight_normalise*weight_mc*weight_pileup*weight_lepton*weight_bTag*weight_jvt;

      outTree->Fill();
    }

    // std::cout << "\t\tCK TEMP:" << iEntry << " / " << nEntries << std::endl;
    if (floor( ((float) (iEntry+1)/(float) nEntries)*10 ) == percentDoneCheck) {
      std::cout << "\t\t" << percentDoneCheck << "0%" << std::endl;
      ++percentDoneCheck;
    }
  }

  std::cout << "\tClosing output file" << std::endl;
  outTree->Write();
  outFile->Close();
  // delete outFile;
  // delete outTree;

  std::cout << "\tNtuple creation finished" << std::endl;
}



void makeAllNtuples() {
  std::cout << "Start of script" << std::endl;

  std::cout << "Creating input info" << std::endl;

  std::string inputDir = "/scratch4/ckilby/ttH_Work/AnalysisTop-2.4.30/ttHScripts/MayProd_v3_withAllTtbarCors_andMCFakes/ttH-offline_Output_merge/ge2jge2b/";
  
  // Modify this vector to run over desired files
  std::vector< info > vecIn;
  // Reminder to self: the "fakes" files have no fakes selection applied, this was done in fit, and so this is just using inclusive files, as requested by combination
  vecIn.push_back( info(inputDir+"ttbar_PP8_lep_fakes_FS.root",
			"ttbar_PoPy8.root") );
  vecIn.push_back( info(inputDir+"ttbar_PP8_lep_bfil_fakes_FS.root",
			"ttbar_PoPy8_bfil.root") );
  vecIn.push_back( info(inputDir+"ttH_aMcP8_FS.root",
			"ttH_aMcPy8.root") );
  vecIn.push_back( info(inputDir+"ttHgamgam_FS.root",
			"ttHgamgam_FS.root") );
  vecIn.push_back( info(inputDir+"ttH4l_FS.root",
			"ttH4l_FS.root") );

  std::cout << "Iterating over inputs for ntuple creation" << std::endl;
  for (info& infoIn : vecIn) {
    makeOverlapNtuple(infoIn);
  }

  std::cout << "\tClosing input files" << std::endl;
  for (info& infoIn : vecIn) { // If input files are somehow shared betweein infos, what happens if you try to close a file a second time?
    infoIn.file->Close();
  }
  
  std::cout << "End of script" << std::endl;
}
